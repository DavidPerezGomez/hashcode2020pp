import bisect


class Solution:

    def __init__(self, selected_values, unused_values, score):
        self.selected_values = selected_values
        self.unused_values = unused_values
        self.score = score

    def __str__(self):
        return '{}\n{}\n{}'.format(
            len(self.selected_values),
            ' '.join([str(i) for i in self.selected_values]),
            self.score)


class Approximator:

    def __init__(self, _target_score, values):
        self._target_score = _target_score
        self._values = values

    @property
    def target_score(self):
        return self._target_score

    @property
    def values(self):
        return self._values

    @staticmethod
    def simple_approx(target_score, options, min_score=-1):
        solution = []
        unused = []
        total_score = 0

        done = False
        i = len(options) - 1
        while i >= 0:
            option_score = options[i]
            if not done and total_score + option_score <= target_score:
                # add the next value in the list
                solution.insert(0, options[i])
                total_score += option_score

                if total_score == target_score:
                    # done!!!
                    done = True
            else:
                unused.insert(0, options[i])

            i -= 1

        if total_score >= min_score:
            return Solution(solution, unused, total_score)
        else:
            return None

    @staticmethod
    def recursive_approx(target_score, options, min_score=-1, depth=1):
        approx = Approximator.simple_approx(target_score, options, min_score)

        if not approx:
            return None

        if depth == 0:
            return approx

        i = len(approx.selected_values) - 1
        while i >= 0:
            if approx.score == target_score:
                return approx

            option_value = approx.selected_values[i]
            margin = target_score - approx.score
            sub_approx = Approximator.recursive_approx(option_value + margin,
                                                       approx.unused_values,
                                                       option_value,
                                                       depth - 1)

            if sub_approx:
                approx.selected_values.remove(option_value)
                bisect.insort(approx.unused_values, option_value)
                approx.score -= option_value

                for new_option in sub_approx.selected_values:
                    bisect.insort(approx.selected_values, new_option)
                    approx.unused_values.remove(new_option)
                    approx.score += new_option

                i += len(sub_approx.selected_values) - 1

            i -= 1

        return approx

    def approximate(self, depth=1):
        # start of recursion
        return Approximator.recursive_approx(self.target_score, self.values, depth=depth)
