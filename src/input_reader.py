from os import listdir
from os import path
import re


INPUT_REL_PATH = '../input'
INPUT_PATH = path.abspath(path.join(path.curdir, INPUT_REL_PATH))


def read_input_file(letter):
    if letter not in ['a', 'b', 'c', 'd', 'e']:
        return None

    pattern = re.compile(r'{}_.*\.in'.format(letter))

    files = [f for f in listdir(INPUT_PATH)
             if path.isfile(path.join(INPUT_PATH, f))
             and pattern.match(f)]

    if not files:
        return None

    input_file_path = path.abspath(path.join(INPUT_PATH, files[0]))

    with open(input_file_path) as input_file:
        return _parse_input_file(input_file)


def _parse_input_file(file):
    lines = file.readlines()

    first_line_values = lines[0].split(' ')
    target_slices = int(first_line_values[0])
    n_pizzas = int(first_line_values[1])

    pizzas = [int(p) for p in lines[1].split(' ')]

    if n_pizzas != len(pizzas):
        raise ValueError('Incorrect number of pizzas! Expected: {}, Found: {}'.format(n_pizzas, len(pizzas)))

    return {
        'target_slices': target_slices,
        'pizzas': pizzas
    }
