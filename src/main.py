from src import input_reader
from src import output_writer
from src.approximator import Approximator


def get_pizzas_indices(pizzas, selected_pizzas):
    min_index = 0
    indices = []
    for pizza in selected_pizzas:
        if indices:
            min_index = indices[-1] + 1
        indices.append(pizzas.index(pizza, min_index))

    return ' '.join([str(i) for i in indices])


def main():
    for letter in ['a', 'b', 'c', 'd', 'e']:
        # read input
        input_value = input_reader.read_input_file(letter)

        # find solution
        a = Approximator(input_value['target_slices'], input_value['pizzas'])
        approx = a.approximate(2)
        print(approx, end='\n\n')

        # write output
        output_file = '{}_output.txt'.format(letter)
        output = '{}\n{}'.format(str(len(approx.selected_values)),
                                 get_pizzas_indices(input_value['pizzas'], approx.selected_values))
        output_writer.write_output(output_file, output)


if __name__ == '__main__':
    main()
